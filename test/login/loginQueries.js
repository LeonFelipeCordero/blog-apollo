export const LOGIN = `
query login($email: String, $password: String) {
    login(email: $email, password: $password) {
        token
        profileId
    }
}
`