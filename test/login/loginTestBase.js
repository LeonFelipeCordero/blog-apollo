import TestBase from '../testBase'
import {LOGIN} from './loginQueries'

export default class LoginTestBase extends TestBase {

    constructor() {
        super()
    }

    async login() {
        return await this.mutate(LOGIN, {
            variables: {
                email: 'john.wick@email.com',
                password: '1234' 
            }
        })
    }

}