import LoginTestBase from './loginTestBase'

const testBase = new LoginTestBase()

beforeAll(function (done) {
    testBase.start(done)
});
afterAll(function (done) {
    testBase.close(done)
})

it('should login', async () => {
    const result = await testBase.login()

    const token = result.data.login

    expect(token.token).not.toBeNull()
    expect(token.token).not.toBeUndefined()
    expect(token.profileId).toBe('123123123')
})