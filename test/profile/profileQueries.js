export const GET_PROFILE = `
query profile($id: String!) {
    profile(id: $id) {
        id
        firstName
        lastName
        userName
        email
        isAuthor
    }
}
`

export const CREATE_PROFILE = `
mutation createProfile($firstName: String, $lastName: String, $userName: String, $email: String, $password: String, $isAuthor: Boolean) {
    createProfile(firstName: $firstName, lastName: $lastName, userName: $userName, email: $email, password: $password, isAuthor: $isAuthor) {
        token
        profileId
    }
}
`

export const BECOME_AUTHOR = `
mutation becomeAuthor($id: String!) {
    becomeAuthor(id: $id) {
        id
        firstName
        lastName
        userName
        email
        isAuthor
    }
}
`

export const UPDATE_PROFILE = `
mutation updateProfile($id: String!, $firstName: String, $lastName: String, $userName: String, $email: String) {
    updateProfile(id: $id, firstName: $firstName, lastName: $lastName, userName: $userName, email: $email) {
        id
        firstName
        lastName
        userName
        email
        isAuthor
    }
}
`

export const DELETE_PROFILE = `
mutation deleteProfile($id: String!) {
    deleteProfile(id: $id) {
        status
        message
        timestamp
    }
}
`