import TestBase from '../testBase'
import {
    GET_PROFILE,
    CREATE_PROFILE,
    DELETE_PROFILE,
    BECOME_AUTHOR,
    UPDATE_PROFILE
} from '../profile/profileQueries'

export default class ProfileTestBase extends TestBase {


    constructor() {
        super()
    }

    async getProfile() {
        return await this.mutate(GET_PROFILE, {
            variables: {
                id: "123123123"
            }
        })
    }

    async createProfile(isAuthor = false) {
        return await this.mutate(CREATE_PROFILE, {
            variables: {
                firstName: 'John',
                lastName: 'Wick',
                userName: 'JohnWick',
                email: 'john.wick@email.com',
                password: '1234',
                isAuthor: isAuthor
            }
        })
    }

    async becomeAuthor() {
        return await this.mutate(BECOME_AUTHOR, {
            variables: {
                id: "123123123"
            }
        })
    }

    async updateProfile() {
        return await this.mutate(UPDATE_PROFILE, {
            variables: {
                id: "123123123",
                firstName: 'John',
                lastName: 'Wick',
                userName: 'JohnWick',
                email: 'john.wick@email.com',
            }
        })
    }

    async deleteProfile(id) {
        return await this.mutate(DELETE_PROFILE, {
            variables: {
                id: "123123123"
            }
        })
    }

}