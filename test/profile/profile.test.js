import ProfileTestBase from './profileTestBase'

const testBase = new ProfileTestBase()

beforeAll(function (done) {
    testBase.start(done)
});
afterAll(function (done) {
    testBase.close(done)
})

it('should get profile', async () => {
    const result = await testBase.getProfile()    

    const profile = result.data.profile

    expect(profile.id).toBe('123123123')
    expect(profile.firstName).toBe('John')
    expect(profile.lastName).toBe('Wick')
    expect(profile.userName).toBe('JohnWick')
    expect(profile.email).toBe('john.wick@email.com')
    expect(profile.isAuthor).toBe(false)
})

it('should create profile', async () => {
    const result = await testBase.createProfile()

    const token = result.data.createProfile

    expect(token.token).not.toBeNull()
    expect(token.token).not.toBeUndefined()
    expect(token.profileId).toBe('123123123')
})

it('should make become author', async () => {
    const result = await testBase.becomeAuthor()

    const profile = result.data.becomeAuthor

    expect(profile.id).toBe('123123123')
    expect(profile.firstName).toBe('John')
    expect(profile.lastName).toBe('Wick')
    expect(profile.userName).toBe('JohnWick')
    expect(profile.email).toBe('john.wick@email.com')
    expect(profile.isAuthor).toBe(true)
})

it('should update profile', async () => {
    const result = await testBase.updateProfile()

    const profile = result.data.updateProfile

    expect(profile.id).toBe('123123123')
    expect(profile.firstName).toBe('Iron')
    expect(profile.lastName).toBe('Man')
    expect(profile.userName).toBe('IronMan')
    expect(profile.email).toBe('iron.man@email.com')
    expect(profile.isAuthor).toBe(false)
})

it('should delete profile', async () => {
    const result = await testBase.deleteProfile()

    const message = result.data.deleteProfile

    expect(message.status).toBe('Profile Deleted')
    expect(message.message).toBe('Profile has been deleted')
    expect(message.timestamp).toBe('2020-02-20T00:00:00.0001')
})