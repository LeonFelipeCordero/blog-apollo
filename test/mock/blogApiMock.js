import ServerMock from 'mock-http-server'

export default class BlogApiMock {

    constructor() {
        this.server = new ServerMock({ host: 'localhost', port: '8080' })
        this.stubForGetProfile()
        this.stubForNewProfile()
        this.stubForBecomeAuthor()
        this.stubForUpdateProfile()
        this.stubToDeleteProfile()
        this.stubForLogin()
        this.stubForCreateArticle()
        this.stubForFilterArticles()
        this.stubForGetArticle()
        this.stubForTopArticles()
        this.stubForAddComment()
        this.stubForUpdateArticle()
        this.stubForDeleteArticle()
    }

    start(done) {
        this.server.start(done)
    }

    stop(done) {
        this.server.stop(done)
    }


    stubForGetProfile() {
        this.server.on({
            method: 'GET',
            path: '/profile/123123123',
            reply: {
                status: 200,
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    id: "123123123",
                    firstName: "John",
                    lastName: "Wick",
                    userName: "JohnWick",
                    email: "john.wick@email.com",
                    isAuthor: false
                })
            }
        })
    }

    stubForNewProfile() {
        this.server.on({
            method: 'POST',
            path: '/profile',
            reply: {
                status: 200,
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    id: "123123123",
                    firstName: "John",
                    lastName: "Wick",
                    userName: "JohnWick",
                    email: "john.wick@email.com",
                    isAuthor: false
                })
            }
        })
    }

    stubForBecomeAuthor() {
        this.server.on({
            method: 'PUT',
            path: '/profile/123123123/author',
            reply: {
                status: 200,
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    id: "123123123",
                    firstName: "John",
                    lastName: "Wick",
                    userName: "JohnWick",
                    email: "john.wick@email.com",
                    isAuthor: true
                })
            }
        })
    }

    stubForUpdateProfile() {
        this.server.on({
            method: 'PUT',
            path: '/profile/123123123',
            reply: {
                status: 200,
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    id: "123123123",
                    firstName: "Iron",
                    lastName: "Man",
                    userName: "IronMan",
                    email: "iron.man@email.com",
                    isAuthor: false
                })
            }
        })
    }

    stubToDeleteProfile() {
        this.server.on({
            method: 'DELETE',
            path: '/profile/123123123',
            reply: {
                status: 200,
                headers: { "content-type": "application/json" },
                body: JSON.stringify({
                    status: "Profile Deleted",
                    message: "Profile has been deleted",
                    timestamp: "2020-02-20T00:00:00.0001"
                })
            }
        })
    }

    stubForLogin() {
        this.server.on({
            method: 'PUT',
            path: '/login',
            status: 200,
            headers: { "content-type": "application/json" },
            reply: {
                body: JSON.stringify({
                    id: "123123123",
                    firstName: "John",
                    lastName: "Wick",
                    userName: "JohnWick",
                    email: "john.wick@email.com",
                    isAuthor: false
                })
            }
        })
    }

    stubForGetArticle() {
        this.server.on({
            method: 'GET',
            path: '/article/1234',
            reply: {
                status: 200,
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({
                    id: '1234',
                    title: 'test post',
                    blogHtml: '<p>test</p>',
                    shortDescription: 'the post description',
                    author: '123123123',
                    category: 'test category',
                    comments: []
                })
            }
        })
    }

    stubForFilterArticles() {
        this.server.on({
            method: 'GET',
            path: '/article?title=title&author=1234&category=category&date=2020-02-20',
            reply: {
                status: 200,
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify([
                    {
                        id: '1234',
                        title: 'test post',
                        blogHtml: '<p>test</p>',
                        shortDescription: 'the post description',
                        author: '123123123',
                        category: 'test category',
                        comments: []
                    },
                    {
                        id: '4321',
                        title: 'test',
                        blogHtml: '<p>second test</p>',
                        shortDescription: 'the second description',
                        author: '123123123',
                        category: 'the second category',
                        comments: []
                    }
                ])
            }
        })
    }

    stubForFilterArticles() {
        this.server.on({
            method: 'GET',
            path: '/article?author=123123123',
            reply: {
                status: 200,
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify([
                    {
                        id: '1234',
                        title: 'test post',
                        blogHtml: '<p>test</p>',
                        shortDescription: 'the post description',
                        author: '123123123',
                        category: 'test category',
                        comments: []
                    },
                    {
                        id: '4321',
                        title: 'test',
                        blogHtml: '<p>second test</p>',
                        shortDescription: 'the second description',
                        author: '123123123',
                        category: 'the second category',
                        comments: []
                    }
                ])
            }
        })
    }

    stubForTopArticles() {
        this.server.on({
            method: 'GET',
            path: '/article/top',
            reply: {
                status: 200,
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify([
                    {
                        id: '1234',
                        title: 'test post',
                        blogHtml: '<p>test</p>',
                        shortDescription: 'the post description',
                        author: '123123123',
                        category: 'test category',
                        comments: []
                    },
                    {
                        id: '4321',
                        title: 'test',
                        blogHtml: '<p>second test</p>',
                        shortDescription: 'the second description',
                        author: '123123123',
                        category: 'the second category',
                        comments: []
                    }
                ])
            }
        })
    }

    stubForCreateArticle() {
        this.server.on({
            method: 'POST',
            path: '/article',
            reply: {
                status: 200,
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({
                    id: '1234',
                    title: 'test post',
                    blogHtml: '<p>test</p>',
                    shortDescription: 'the post description',
                    author: '123123123',
                    category: 'test category',
                    comments: []
                })
            }
        })
    }

    stubForAddComment() {
        this.server.on({
            method: 'PUT',
            path: '/article/1234/comment',
            reply: {
                status: 200,
                header: { 'content-type': 'application/json' },
                body: JSON.stringify({
                    status: "Comment added",
                    message: "Comment has been added to article",
                    timestamp: "2020-02-20T00:00:00.0001"
                })
            }
        })
    }

    stubForUpdateArticle() {
        this.server.on({
            method: 'PUT',
            path: '/article/1234',
            reply: {
                status: 200,
                header: { 'content-type': 'application/json' },
                body: JSON.stringify({
                    id: '1234',
                    title: 'post test',
                    blogHtml: '<h1>test</h1>',
                    shortDescription: 'the description post',
                    author: '123123123',
                    category: 'category test',
                    comments: []
                })
            }
        })
    }

    stubForDeleteArticle() {
        this.server.on({
            method: 'DELETE',
            path: '/article/1234',
            reply: {
                status: 200,
                header: { 'content-type': 'application/json' },
                body: JSON.stringify({
                    status: "Article deleted",
                    message: "Article has been deleted",
                    timestamp: "2020-02-20T00:00:00.0001"
                })
            }
        })
    }
}