import ArticleTestBase from './articleTestBase'

const testBase = new ArticleTestBase()

beforeAll(function (done) {
    testBase.start(done)
});
afterAll(function (done) {
    testBase.close(done)
})

it('should get article', async () => {
    const result = await testBase.getArticle()

    const article = result.data.article

    expect(article.id).toBe('1234')
    expect(article.title).toBe('test post')
    expect(article.blogHtml).toBe('<p>test</p>')
    expect(article.shortDescription).toBe('the post description')
    expect(article.author.id).toBe('123123123')
    expect(article.category).toBe('test category')
})

// todo not working :'(
// it('should filter articles', async () => {
//     const result = await testBase.filterArticles()

//     console.log(result);

//     const articles = result.data.filterArticles

//     expect(articles).toHaveLength(2)
// expect(article.id).toBe('1234')
// expect(article.title).toBe('test post')
// expect(article.blogHtml).toBe('<p>test</p>')
// expect(article.shortDescription).toBe('the post description')
// expect(article.author).toBe('1234')
// expect(article.category).toBe('test category')
// })

it('should get top articles', async () => {
    const result = await testBase.topArticles()

    const articles = result.data.topArticles

    expect(articles).toHaveLength(2)
})

it('should create new article', async () => {
    const result = await testBase.createArticle()

    const article = result.data.createArticle

    expect(article.id).toBe('1234')
    expect(article.title).toBe('test post')
    expect(article.blogHtml).toBe('<p>test</p>')
    expect(article.shortDescription).toBe('the post description')
    expect(article.author.id).toBe('123123123')
    expect(article.category).toBe('test category')
})

it('should add comment to article', async () => {
    const result = await testBase.addComment()

    const response = result.data.addComment

    expect(response.status).toBe('Comment added')
    expect(response.message).toBe('Comment has been added to article')
    expect(response.timestamp).toBe('2020-02-20T00:00:00.0001')
})

it('should update article', async () => {
    const result = await testBase.updateArticle()

    const article = result.data.updateArticle

    expect(article.id).toBe('1234')
    expect(article.title).toBe('post test')
    expect(article.blogHtml).toBe('<h1>test</h1>')
    expect(article.shortDescription).toBe('the description post')
    expect(article.author.id).toBe('123123123')
    expect(article.category).toBe('category test')
})

it('should delete article', async () => {
    const result = await testBase.deleteArticle()

    const response = result.data.deleteArticle

    expect(response.status).toBe('Article deleted')
    expect(response.message).toBe('Article has been deleted')
    expect(response.timestamp).toBe('2020-02-20T00:00:00.0001')
})