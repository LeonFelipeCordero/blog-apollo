export const GET_ARTICLE = `
query article($id: String!) {
    article(id: $id) {
        id
        title
        blogHtml
        shortDescription
        author {
            id
            firstName
            lastName
            userName
            email
        }
        category
    }
}
`

export const FILTER_ARTICLES = `
query filterArticles($title: String!, $author: String!, $category: String!, $date: String) {
    filterArticles(title: $title, author: $author, category: $category, date: $date) {
        id
        title
        blogHtml
        shortDescription
        author {
            id
            firstName
            lastName
            userName
            email
        }
        category
    }
}
`

export const TOP_ARTICLES = `
query topArticles {
    topArticles {
        id
        title
        blogHtml
        shortDescription
        author {
            id
            firstName
            lastName
            userName
            email
        }
        category
    }
}
`

export const CREATE_ARTICLE = `
mutation createArticle($title: String!, $blogHtml: String!, $shortDescription: String!, $author: String!, $category: String!) {
    createArticle(title: $title, blogHtml: $blogHtml, shortDescription: $shortDescription, author: $author, category: $category) {
        id
        title
        blogHtml
        shortDescription
        author {
            id
            firstName
            lastName
            userName
            email
        }
        category
    }
}
`

export const ADD_COMMENT = `
mutation addComment($id: String!, $profile: String!, $message: String!) {
    addComment(id: $id, profile: $profile, message: $message) {
        status
        message
        timestamp
    }
}
`

export const UPDATE_ARTICLE = `
mutation updateArticle($id: String!, $title: String!, $blogHtml: String!, $shortDescription: String!, $category: String!) {
    updateArticle(id: $id, title: $title, blogHtml: $blogHtml, shortDescription: $shortDescription, category: $category) {
        id
        title
        blogHtml
        shortDescription
        author {
            id
            firstName
            lastName
            userName
            email
        }
        category
    }
}
`

export const DELETE_ARTICLE = `
mutation deleteArticle($id: String!) {
    deleteArticle(id: $id) {
        status
        message
        timestamp   
    }
}
`