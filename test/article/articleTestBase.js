import TestBase from '../testBase'
import {
    CREATE_ARTICLE,
    GET_ARTICLE,
    FILTER_ARTICLES,
    TOP_ARTICLES,
    ADD_COMMENT,
    UPDATE_ARTICLE,
    DELETE_ARTICLE
} from './articleQueries'

export default class ArticleTestBase extends TestBase {

    constructor() {
        super()
    }

    async getArticle() {
        return await this.query(GET_ARTICLE, {
            variables: {
                id: '1234'
            }
        })
    }

    async filterArticles() {
        return await this.query(FILTER_ARTICLES, {
            variables: {
                title: 'title',
                author: '1234',
                category: 'category',
                date: '2020-02-20'
            }
        })
    }

    async topArticles() {
        return await this.query(TOP_ARTICLES)
    }

    async createArticle() {
        return await this.mutate(CREATE_ARTICLE, {
            variables: {
                title: 'test post',
                blogHtml: '<p>test</p>',
                shortDescription: 'the post description',
                author: '1234',
                category: 'test category'
            }
        })
    }

    async addComment() {
        return await this.mutate(ADD_COMMENT, {
            variables: {
                id: '1234',
                profile: '1234',
                message: 'this is a comment'
            }
        })
    }

    async updateArticle() {
        return await this.mutate(UPDATE_ARTICLE, {
            variables: {
                id: '1234',
                title: 'test post',
                blogHtml: '<p>test</p>',
                shortDescription: 'the post description',
                category: 'test category'
            }
        })
    }

    async deleteArticle() {
        return await this.mutate(DELETE_ARTICLE, {
            variables: {
                id: '1234'
            }
        })
    }
}