import { createTestClient } from 'apollo-server-integration-testing';
import apolloServer from '../src/server'
import BlogApiMock from './mock/blogApiMock'
    
export default class TestBase {

    constructor() {
        const { query, mutate, setOptions } = createTestClient({
            apolloServer
        })
        this.query = query
        this.mutate = mutate
        this.setOptions = setOptions
        this.apiMock = new BlogApiMock()
    }

    async close(done) {
        await apolloServer.stop()
        this.apiMock.stop(done)
    }

    start(done) {
        this.apiMock.start(done)
    }

    auth(token) {
        this.setOptions({
            request: {
                headers: {
                    token: token
                }
            }
        })
    }
}