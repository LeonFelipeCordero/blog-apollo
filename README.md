# Apollo Blog API

### Setup
`yarn`

### Run
`yarn dev`

### Test
`yarn test test/ --runInBand --forceExit`

### run on k8s
Before deploying the blog-frontend to a k8s-cluster make sure you follow the instruction in https://gitlab.com/LeonFelipeCordero/blog-api about how to create the k8s environment.

Ones mongodb and blog-api are running you can deploy the frontend

```
kubectl apply -f ci/k8s/deployment.yml
```
