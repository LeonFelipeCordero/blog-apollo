import express from 'express'
import apolloServer from './server'

const app = express()

apolloServer.applyMiddleware({ app, path: '/graphql' })

app.listen(4000, (url) => console.log(`🚀 app running graphql`))
