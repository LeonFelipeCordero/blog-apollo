import ArticleService from "../services/articleService"
import ProfileService from "../services/profileService"

export default {
    Query: {
        article: async (_, { id }, { dataSources: { blogApi } }) => {
            const articleService = new ArticleService(blogApi)

            return await articleService.get(id)
        },
        filterArticles: async (_, { title, author, category, date }, { dataSources: { blogApi } }) => {
            const articleService = new ArticleService(blogApi)

            return await articleService.filter(title, author, category, date)
        },
        topArticles: async (_, { }, { dataSources: { blogApi } }) => {
            const articleService = new ArticleService(blogApi)

            return await articleService.topArticles()
        }
    },
    Mutation: {
        createArticle: async (_, { title, blogHtml, shortDescription, author, category }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }

            const articleService = new ArticleService(blogApi)

            return await articleService.create(title, blogHtml, shortDescription, author, category)

        },
        addComment: async (_, { id, profile, message }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }

            const articleService = new ArticleService(blogApi)

            return await articleService.addComment(id, profile, message)

        },
        updateArticle: async (_, { id, title, blogHtml, shortDescription, author, category }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }

            const articleService = new ArticleService(blogApi)

            return await articleService.update(id, title, blogHtml, shortDescription, author, category)

        },
        deleteArticle: async (_, { id }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }
            
            const articleService = new ArticleService(blogApi)

            return await articleService.delete(id)
        },
    },
    Article: {
        author: async (parent, args, { dataSources: { blogApi } }, info) => {
            const profileService = new ProfileService(blogApi)

            return await profileService.get(parent.author)
        }
    }
}