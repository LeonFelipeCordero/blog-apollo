import profileResolver from './profile'
import loginResolver from './profile'
import articleResolver from './article'

export default [profileResolver, loginResolver, articleResolver]
