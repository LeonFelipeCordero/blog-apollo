import jwt from 'jsonwebtoken'
import ProfileService from '../services/profileService'
import LoginService from '../services/loginService'
import ArticleService from '../services/articleService'

export default {
    Query: {
        profile: async (_, { id }, { dataSources: { blogApi }, me }) => {
            const profileService = new ProfileService(blogApi)

            return await profileService.get(id)
        },
        login: async (_, { email, password }, { dataSources: { blogApi } }) => {
            const loginService = new LoginService(blogApi)

            const profile = await loginService.login(email, password)

            if (!profile) {
                throw new Error("Something went wrong during login")
            }

            const token = jwt.sign({ profileId: profile.id }, 'riddlemethis', { expiresIn: 24 * 10 * 50 })

            return {
                token: token,
                profileId: profile.id
            }
        }
    },
    Mutation: {
        createProfile: async (_, { firstName, lastName, userName, email, password, isAuthor }, { dataSources: { blogApi } }) => {
            const profileService = new ProfileService(blogApi)

            const profile = await profileService.create(firstName, lastName, userName, email, password, isAuthor)

            if (!profile) {
                throw new Error("Something went wrong creating the new profile")
            }

            const token = jwt.sign({ profileId: profile.id }, 'riddlemethis', { expiresIn: 24 * 10 * 50 })

            return {
                token: token,
                profileId: profile.id
            }
        },
        becomeAuthor: async (_, { id }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }

            const profileService = new ProfileService(blogApi)

            return await profileService.becomeAuthor(id)
        },
        updateProfile: async (_, { id, firstName, lastName, userName, email, }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }

            const profileService = new ProfileService(blogApi)

            return await profileService.update(id, firstName, lastName, userName, email)
        },
        deleteProfile: async (_, { id }, { dataSources: { blogApi }, me }) => {
            // if (!me) {
            //     throw new AuthenticationError('You are not authenticated')
            // }

            const profileService = new ProfileService(blogApi)

            return await profileService.delete(id)
        },
    },
    Profile: {
        articles: async (parent, args, { dataSources: { blogApi } }, info) => {
            const articleService = new ArticleService(blogApi)

            return await articleService.filter({ author: parent.id })
        }
    }
}