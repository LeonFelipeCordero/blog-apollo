import jwt from 'jsonwebtoken'
import LoginService from '../services/loginService'

export default {
    Query: {
        login: async (_, { email, password }, { dataSources: { blogApi } }) => {
            const loginService = new LoginService(blogApi)

            const profile = await loginService.login(email, password)

            if (!profile) {
                throw new Error("Something went wrong during login")
            }

            const token = jwt.sign({ profileId: profile.id }, 'riddlemethis', { expiresIn: 24 * 10 * 50 })

            return {
                token: token,
                profileId: profile.id
            }
        }
    }
}