
const { RESTDataSource } = require('apollo-datasource-rest');

export default class BlogApi extends RESTDataSource {

    constructor() {
        super();
        this.baseURL = process.env.BLOG_API_HOST || 'http://localhost'
        this.paths = {
            profile: {
                base: '/profile',
                id: '/profile/$id',
                author: '/profile/$id/author'
            },
            login: '/login',
            article: {
                base: '/article',
                id: '/article/$id',
                top: '/article/top',
                comment: '/article/$id/comment',
            }
        }
    }

    async createProfile(profileRequest) {
        return await this.post(
            this.paths.profile.base,
            profileRequest
        )
    }

    async getProfile(id) {
        return await this.get(
            this.paths.profile.id.replace('$id', id)
        )
    }

    async becomeAuthor(id) {
        return await this.put(
            this.paths.profile.author.replace('$id', id)
        )
    }

    async updateProfile(id, profileRequest) {
        return await this.put(
            this.paths.profile.id.replace('$id', id),
            profileRequest
        )
    }

    async deleteProfile(id) {
        return await this.delete(
            this.paths.profile.id.replace('$id', id)
        )
    }

    async login(loginRequest) {
        return await this.put(
            this.paths.login,
            loginRequest
        )
    }

    async getArticleById(id) {
        return await this.get(
            this.paths.article.id.replace('$id', id)
        )
    }

    async filterArticles(parameters) {
        const queryParameters = this.preparePath(parameters)
        console.log(this.paths.article.base + '?' + queryParameters);
                
        return await this.get(this.paths.article.base + '?' + queryParameters)
    }

    async topArticles() {
        return await this.get(this.paths.article.top)
    }

    async createArticle(articleRequest) {
        return await this.post(
            this.paths.article.base,
            articleRequest
        )
    }

    async addComment(id, commentRequest) {
        return await this.put(
            this.paths.article.comment.replace('$id', id),
            commentRequest
        )
    }

    async updateArticle(id, articleRequest) {
        return await this.put(
            this.paths.article.id.replace('$id', id),
            articleRequest
        )
    }

    async deleteArticle(id) {
        return await this.delete(
            this.paths.article.id.replace('$id', id)
        )
    }

    preparePath(parameters) {
        let query = []        
        for (let parameter in parameters)
            if (parameters[parameter] !== undefined && parameters[parameter] !== null) {
                query.push(parameter + '=' + parameters[parameter])
            }
        return query.join('&')
    }
}