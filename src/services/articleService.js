export default class ArticleService {

    constructor(blogApi) {
        this.blogApi = blogApi
    }

    async get(id) {
        return await this.blogApi.getArticleById(id)
    }

    async filter(parameters) {
        return await this.blogApi.filterArticles(parameters)
    }

    async topArticles() {
        return await this.blogApi.topArticles()
    }

    async create(title, blogHtml, shortDescription, author, category) {
        return await this.blogApi.createArticle({
            title: title,
            blogHtml: blogHtml,
            shortDescription: shortDescription,
            author: author,
            category: category
        })
    }

    async addComment(id, profile, message) {
        return await this.blogApi.addComment(id, {
            comment: {
                profile: profile,
                content: message
            }
        })
    }

    async update(id, title, blogHtml, shortDescription, category) {
        return await this.blogApi.updateArticle(id, {
            title: title,
            blogHtml: blogHtml,
            shortDescription: shortDescription,
            category: category
        })
    }

    async delete(id) {
        return await this.blogApi.deleteArticle(id)
    }
} 