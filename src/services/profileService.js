export default class ProfileService {

    constructor(blogApi) {
        this.blogApi = blogApi
    }

    async create(firstName, lastName, userName, email, password, isAuthor) {
        return await this.blogApi.createProfile({
            firstName: firstName,
            lastName: lastName,
            userName: userName,
            email: email,
            password: password,
            isAuthor: isAuthor
        })
    }

    async get(id) {
        return await this.blogApi.getProfile(id)
    }

    async becomeAuthor(id) {
        return await this.blogApi.becomeAuthor(id)
    }

    async update(id, firstName, lastName, userName, email) {
        return await this.blogApi.updateProfile(id, {
            firstName: firstName,
            lastName: lastName,
            userName: userName,
            email: email
        })
    }

    async delete(id) {
        return await this.blogApi.deleteProfile(id)
    }
} 