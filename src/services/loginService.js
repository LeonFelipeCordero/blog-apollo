export default class LoginService {

    constructor(blogApi) {
        this.blogApi = blogApi
    }

    async login(email, password) {
        return await this.blogApi.login({
            email: email,
            password: password
        })
    }
} 