import jwt from 'jsonwebtoken'
import { ApolloServer, AuthenticationError } from 'apollo-server-express'
import typeDefs from './schema'
import resolvers from './resolvers'
import BlogApi from './datasources/blogApi'
import ProfileService from './services/profileService'
import LoginService from './services/loginService'

const getUser = async (req) => {
    const token = req.headers['token']
    if (token) {
        try {
            return jwt.verify(token, 'riddlemethis')
        } catch (e) {
            throw new AuthenticationError('Your session expired. Sign in again.')
        }
    }
};

const blogApi = new BlogApi()

const apolloServer = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({ req }) => {
        if (req) {
            const me = await getUser(req)
            return { me }
        }
    },
    dataSources: () => ({
        blogApi: new BlogApi()
    })
    // dataSources: () => ({
    //     profileService: new ProfileService(blogApi),
    //     loginService: new LoginService(blogApi)
    // })
})

export default apolloServer