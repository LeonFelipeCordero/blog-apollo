FROM node:14.1.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn 
# for prod RUN npm ci --only=production https://blog.npmjs.org/post/171556855892/introducing-npm-ci-for-faster-more-reliable
# RUN npm install --production

COPY . .

RUN yarn build

EXPOSE 4000

CMD ["yarn", "serve"]
